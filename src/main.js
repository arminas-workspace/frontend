import Vue from 'vue';
import App from './App.vue';
import { router } from './router';
import store from './store';
import 'bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/css/bootstrap.css';
import "vue-select/dist/vue-select.css";
import VeeValidate from 'vee-validate';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faHome,
  faUser,
  faUserPlus,
  faSignInAlt,
  faSignOutAlt
} from '@fortawesome/free-solid-svg-icons';
import VueSimpleAlert from "vue-simple-alert";
import Dropdown from 'hsy-vue-dropdown';
import vSelect from 'vue-select';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import VueMoment from 'vue-moment';
import moment from 'moment-timezone';

library.add(faHome, faUser, faUserPlus, faSignInAlt, faSignOutAlt);


Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(VeeValidate);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.use(VueSimpleAlert);
Vue.use(Dropdown);
Vue.use(VueMoment, { moment, });
Vue.component('v-select', vSelect);
Vue.component('v-select', vSelect.VueSelect);
// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)


new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app');