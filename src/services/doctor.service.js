import axios from 'axios';
import authHeader from './auth-header';

const API_URL = 'https://localhost:3002/doctors';

class DoctorService {
  getAllDoctors() {
    return axios.get(API_URL, { headers: authHeader() });
  }

  getUserBoard() {
    return axios.get(API_URL + 'users', { headers: authHeader() });
  }

  getModeratorBoard() {
    return axios.get(API_URL + 'mod', { headers: authHeader() });
  }

  getAdminBoard() {
    return axios.get(API_URL + 'admin', { headers: authHeader() });
  }
}

export default new DoctorService();