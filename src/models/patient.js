export default class User {
    constructor(name, surname, phone, fk_idUser) {
      this.name = name;
      this.surname = surname;
      this.phone = phone;
      this.fk_idUser = fk_idUser;
    }
  }