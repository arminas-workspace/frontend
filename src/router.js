import Vue from 'vue';
import Router from 'vue-router';
import Home from './views/Home.vue';
import Login from './views/Login.vue';
import Register from './views/Register.vue';
import NotFound from './views/NotFound.vue';

Vue.use(Router);

function requireUser(to, from, next){
  var currentUser = JSON.parse(localStorage.getItem('user'));

  if (currentUser && currentUser.roles) {
    if(currentUser.roles[0].role.includes(to.meta.permission)){
      next();
    }
    else {
      next('*');
    }
  }
}

export const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/home',
      component: Home
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/profile',
      name: 'profile',
      // lazy-loaded
      component: () => import('./views/Profile.vue')
    },
    {
      path: '/users',
      name: 'users',
      beforeEnter : requireUser,
      meta: {
        permission : 'ROLE_ADMIN'
      },
      // lazy-loaded
      component: () => import('./views/Users.vue')
    },
    {
      path: '/patient',
      name: 'patient',
      beforeEnter : requireUser,
      meta: {
        permission : 'ROLE_USER'
      },
      //lazy-loaded
      component: () => import('./views/Patient.vue')
    },
    {
      path: '/patients',
      name: 'patients',
      beforeEnter : requireUser,
      meta: {
        permission : 'ROLE_ADMIN'
      },
      //lazy-loaded
      component: () => import('./views/Patients.vue')
    },
    {
      path: '/doctors',
      name: 'doctors',
      beforeEnter : requireUser,
      meta: {
        permission : 'ROLE_ADMIN'
      },
      // lazy-loaded
      component: () => import('./views/Doctors.vue')
    },
    {
      path: '/selectDoctor',
      name: 'selectDoctor',
      beforeEnter : requireUser,
      meta: {
        permission : 'ROLE_PATIENT'
      },
      // lazy-loaded
      component: () => import('./views/SelectDoctor.vue')
    },
    {
      path: '/visits',
      name: 'visits',
      beforeEnter : requireUser,
      meta: {
        permission : 'ROLE_DOCTOR'
      },
      // lazy-loaded
      component: () => import('./views/Visits.vue')
    },
    {
      path: '/visitsPatient',
      name: 'visitsPatient',
      beforeEnter : requireUser,
      meta: {
        permission : 'ROLE_PATIENT'
      },
      // lazy-loaded
      component: () => import('./views/VisitsPatient.vue')
    },
    {
      path: '/visitsHistory',
      name: 'visitsHistory',
      beforeEnter : requireUser,
      meta: {
        permission : 'ROLE_PATIENT'
      },
      // lazy-loaded
      component: () => import('./views/VisitsHistory.vue')
    },
    { path: "*", component: NotFound },
  ]
});

router.beforeEach((to, from, next) => {
    const publicPages = ['/login', '/register', '/home'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user');

    // trying to access a restricted page + not logged in
    // redirect to login page
    if (authRequired && !loggedIn) {
      next('/login');
    } else {
      next();
    }
  });